import { Routes, Route } from 'react-router-dom';

import CreateRoom from './components/CreateRoom';
import Room from './components/Room';

function App() {
  return (
    <Routes>
      <Route path="/" exact element={<CreateRoom />}></Route>
      <Route path="/room/:roomID" element={<Room />}></Route>
    </Routes>
  );
}

export default App;
